CREATE TABLE "neodyme_magnethashes" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "xt" VARCHAR(64) NOT NULL, -- (commonly "bitdht")
    "hash" VARCHAR(512) UNIQUE NOT NULL,
    "length" BIGINT,
    "displaynames" TEXT,	-- (comma seperated rowids from displaynames table)
    "asource" TEXT,
    "xsource" TEXT,
    "timestamp" DATETIME,	-- (time of first hash insert) 
    "modified" DATETIME,	-- (time of update, added keywords, trackers etc.)
    "keywords" TEXT,	-- (comma seperated rowids from keywords table)
    "manifest" TEXT,
    "trackers" TEXT,	-- (comma seperated rowids from torrent_trackers table)
    "magnetlink" TEXT,
    "originurl" TEXT
);
CREATE TABLE "neodome_torrenttrackers" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "address" TEXT UNIQUE NOT NULL
);

CREATE TABLE "neodyme_displaynames" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "display_name" TEXT UNIQUE NOT NULL
);


;
CREATE TABLE sqlite_sequence(name,seq);
