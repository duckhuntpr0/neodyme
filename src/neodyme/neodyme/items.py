# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field

class NeodymeItem(Item):
    # define the fields for your item here like:
    # name = Field()
    pass

class MagnetLink(Item):
    magnetlink = Field()
    xthash = Field()
    xturntype= Field()
    xtrackers = Field()
    dname = Field()
    keywords = Field()
    description = Field()
    origin_link = Field()
    
