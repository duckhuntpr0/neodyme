# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import logging
import json
import sqlalchemy as sqlal
from scrapy.exceptions import DropItem
from urllib.parse import unquote_plus as UrlUnquote
from .magnetpy import parse_magnet as parse_MagnetURL
from scrapy import item
from .settings import SQLITE_DATABASE as SQLITE_DATABASE
#from BeautifulSoup import BeautifulStoneSoup


class NeodymePipeline(object):
    """Set item data/fields"""
    def process_item(self, item, spider):
        
        self.set_item_hash(item)
        self.set_item_urn_type(item)
        self.set_item_displayname(item)
        self.set_item_trackers(item)
        
        #decide wheter or not to drop item out of pipeline at this point
        if item['xthash'] == False:
            #with no hash present, it's useless to keep around
            raise DropItem("Dropped item : %s" % 'Contains no hash!')
        else:
            return item
    
    def set_item_hash(self,item):
        """extract, check and set the items hash from item's magnetlink"""
        
        #xt key exist in data dict
        if 'xt' in parse_MagnetURL( item['magnetlink']).data.keys():
            #rfind to ":" and set hash
            hash_found = parse_MagnetURL( item['magnetlink']).data['xt'][parse_MagnetURL( item['magnetlink']).data['xt'].rfind(':')+1:]
            item['xthash'] = hash_found
            return True
        else:
            item['xthash'] = False
            return False
                
    def set_item_displayname(self,item):
        """extract and set items display name from item's magnetlink. Or set it to empty string"""
        if 'dn' in parse_MagnetURL( item['magnetlink']).data.keys():
            #magnet link has dn/displayname
            item['dname'] = parse_MagnetURL(item['magnetlink']).data['dn']
            return True #success
        
        else:

            logging.warning("Magnet link does not have a display name")
            
            item['dname'] = str("")
    
    def set_item_urn_type(self,item):
        #xt key exist in data dict
        if 'xt' in parse_MagnetURL( item['magnetlink']).data.keys():
            item['xturntype'] = parse_MagnetURL( item['magnetlink']).data['xt'][4 : parse_MagnetURL( item['magnetlink']).data['xt'].rfind(':') ]
            return True
        else:
            return False
    
    def set_item_trackers(self,item):
        """extract and set items tracker from item's magnetlink. Or set it to empty list"""
        #TODO: check if field contains _any_ data at all
        item['xtrackers'] = list() #make item 'xtracker' field into list() type
        
        if 'tr' in parse_MagnetURL( item['magnetlink']).data.keys() and type(item['xtrackers']) is list :
            for tracker in parse_MagnetURL( item['magnetlink']).data['tr']:
                item['xtrackers'].append( str(tracker) )
            return True
        else:
            item['xtrackers'] = list() #just an empty list()
            return False
                        
                        
class SQLitePipeline(object):
    engine = None
    logging = logging.getLogger("SQLite Pipeline")
    
    def open_spider(self,spider):
        self.engine = sqlal.create_engine(SQLITE_DATABASE, echo=True)
        
    def process_item(self, item, spider):
              
        connection = self.engine.connect()
        #check if items (urntype + xthash) already exist
        
        query_hash_exist = """SELECT COUNT(1) FROM magnetinfo WHERE hash=="{hash}";""".format(hash=item['xthash'])
        #SELECT COUNT(1) FROM magnetinfo WHERE hash="061378b2a94c21d8d790eb24f5322481edc933af";
        
        if not connection.execute(query_hash_exist).fetchone()[0]:
            #print(parse_MagnetURL(item['magnetlink']).data)
            if item['dname']:
                insert_sql = """INSERT INTO magnetinfo (xt,hash,displayname,magnetlink,originurl) VALUES ("{xturntype}","{hash}","{displayname}","{maglink}","{origin}");""".format(xturntype = item['xturntype'],
                                                                                                                                                                                    hash= item['xthash'],
                                                                                                                                                                                    displayname = item['dname'],
                                                                                                                                                                      maglink = item['magnetlink'],
                                                                                                                                                                                    origin = item['origin_link']
                                                                                                                                                                                    )
                #insert into database
                connection.execute(insert_sql)
                self.logging.debug("Stored: {data}".format(data=parse_MagnetURL(item['magnetlink']).data))
        
        else:
            self.logging.info( "Skipping Item: (Hash [{itemhash}] already exist in database)".format(itemhash=item['xthash']) )
            
 
        self.store_item_trackers(item)
            #TODO: if it exists, check for empty fields
                #insert missing (/updated?) data
            
            #if it exist
        return item
        
    def store_item_trackers(self,item):
        """Store trackers found in the to database"""
        if type(item['xtrackers']) is list and len(item['xtrackers']) > 0:
            connection = self.engine.connect()
            for tracker in item['xtrackers']:
                query_tracker_address_exist = """SELECT COUNT(1) FROM torrent_trackers WHERE address=="{trackeraddress}";""".format(trackeraddress=tracker)
            
                if not connection.execute(query_tracker_address_exist).fetchone()[0]:
                    insert_sql = """INSERT INTO torrent_trackers (address) VALUES ("{trackeraddress}");""".format(trackeraddress = tracker)
                    connection.execute(insert_sql)
                    self.logging.debug("Tracker stored in database: {data}".format( data=str(tracker) ) )
                else:
                    self.logging.debug("Tracker already exist in database: {data}".format( data= str(tracker) ) )
                
                #print(tracker)
        
class JSONPipeline(object):

    def open_spider(self, spider):
        self.file = open('/root/volume/items.jl', 'a')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(dict(item)) + "\n"
        self.file.write(line)
        return item
    
    
class DuplicatesPipeline(object):

    def __init__(self):
        self.hashes_seen = set()

    def process_item(self, item, spider):
        if item['hash'] in self.ids_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.ids_seen.add(item['id'])
            return item
        
        
class TheSewer(object):
    def process_item(self, item, spider):
         raise DropItem("Dropped item in sewer: %s" % item['xthash'])
    
