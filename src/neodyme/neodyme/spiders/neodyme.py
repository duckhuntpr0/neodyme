# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider,Rule
from scrapy.contrib.linkextractors import LinkExtractor
from bs4 import BeautifulSoup
from neodyme.items import *
#from neodyme.settings import ACCEPTED_XT

#MAGNET_REGEX= """//a*href=.magnet:\?xt=urn:btih:[a-zA-Z0-9]*"""

class NeodymeMainSpider(CrawlSpider):
    name = 'Neodyme'
    allowed_domains = ['pirateproxy.sh',
                       'tpb.network',
                       'uj3wazyk5u4hnvtk.onion', #piratebay
                       'ekmagnetssemberm.onion',
                       '4444633633feeliz.onion',
                       'jtm5j25w7fq5tubs.onion',
                       'magn37z4yo7mhylp.onion',
                       'torrentzwealmisr.onion/', #torrentz2.to
                        '3g2upl4pq6kufc4m.onion',
                       'seedpeer.eu',
                       'limetorrents.cc',
                       #'magnets.i2p',
                       '4444633633feeliz.onion',
                       'kat.rip',
                       'limetorrents.com',
                       'limetorrents.unblocked.mx/',
                       'bitlord.me',
                       'btbit.org',
                       'www.btsay.org',
                       'btray.org',
                       'magnetdl.com',
                       'yts.am',
                       'yify-torrents.com',
                       'torrents.me',
                       'torrentz2.eu',
                       'thehashden.org',
                       'monova.org',
                       'monova.to',
                       'newlimetorrents.com',
                       'btstorrents.com',
                       'torrentants.com',
                       'javdeep.com',
                       'dailyjavzz.com',
                       'nyaa.se',
                       'btdb.to',
                       'torrentdownloads.me',
                       'tokyotosho.info',
                       'katproxy.com',
                       'kickass.unblocked.mx',
                       'tpbproxy.nl',
                       '1337x.unblocked.mx',
                       'kickass.unblocked.mx',
                       'magnetdl.unblocked.mx',
                       'demonoid.unblocked.m',
                       'eztv.unblocked.mx',
                       'torlock.unblocked.mx'
                       ]
    start_urls = [#'file:///root/volume/torrent_dump_2017.html01',
                  #'http://uj3wazyk5u4hnvtk.onion/static/dump/',
                  'https://torlock.unblocked.mx/fresh.html',
                  'https://kickass.unblocked.mx/browse/',
                  'https://magnetdl.unblocked.mx/download/movies/',
                  'https://limetorrents.unblocked.mx/',
                  'https://eztv.unblocked.mx',
                  'https://eztv.unblocked.mx/calendar/',
                  'http://uj3wazyk5u4hnvtk.onion/recent',
                  'https://tpb.network/recent',
                  'https://tpbproxy.nl/',
                  'http://torrentzwealmisr.onion/search',
                  'http://torrentzwealmisr.onion/my',
                  'http://kat.rip/browse/',
                  'http://www.torrentants.com/new',
                  'http://uj3wazyk5u4hnvtk.onion/tv/latest/',
                  'http://uj3wazyk5u4hnvtk.onion/tag/Movie',
                  'http://uj3wazyk5u4hnvtk.onion/tag/rom',
                  'http://uj3wazyk5u4hnvtk.onion/tag/games',
                  'http://uj3wazyk5u4hnvtk.onion/',
                  'https://www.tokyotosho.info/details.php?id=1206422',
                  'https://www.javdeep.com/released',
                  'https://magnetdl.unblocked.mx/download/',
                  'https://yts.am/browse-movies',
                  'https://www.limetorrents.cc/browse-torrents/Movies/',
                  'http://ekmagnetssemberm.onion/vj.php',
                  'http://ekmagnetssemberm.onion/e421bf49cab8d22ad88aaa279c4a5e0bda42b21c',
                  'http://ekmagnetssemberm.onion/q.php?ts=1522126022&ex=&q=divx&c%5B%5D=1&c%5B%5D=2&c%5B%5D=3&c%5B%5D=4&c%5B%5D=5&c%5B%5D=6&c%5B%5D=7&c%5B%5D=8',
                  'http://en.btbit.org/history.html',
                  'http://en.btbit.org/recommend.html',
                  'http://www.btsay.org/',
                  'http://www.btsay.org/page/720p',
                  'http://www.thehashden.org',
                  'https://btdb.to/recent',
                  'https://www.torrentdownloads.me/',
                  #'https://www.seedpeer.eu/home'
                  ]
    
    proxy = "127.0.0.1:8118"
    
    rules = [
        Rule(LinkExtractor( allow=(),deny=(["btdb.to/about", "onion/rss","/download.php?",'.onion/help','.onion/login','/login.php']),
            deny_extensions=[
                                # images
                                'mng', 'pct', 'bmp', 'gif', 'jpg', 'jpeg', 'png', 'pst', 'psp', 'tif',
                                'tiff', 'ai', 'drw', 'dxf', 'eps', 'ps', 'svg',
                            
                                # audio
                                'mp3', 'wma', 'ogg', 'wav', 'ra', 'aac', 'mid', 'au', 'aiff',
                            
                                # video
                                '3gp', 'asf', 'asx', 'avi', 'mov', 'mp4', 'mpg', 'qt', 'rm', 'swf', 'wmv',
                                'm4a', 'm4v', 'flv', 'mkv'
                            
                                # office suites
                                'xls', 'xlsx', 'ppt', 'pptx', 'pps', 'doc', 'docx', 'odt', 'ods', 'odg',
                                'odp',
                            
                                # other
                                'css', 'pdf', 'exe', 'bin', 'rss', 'zip', 'rar', 'gz', 'pdf', 'torrent',
                                ],
            
            unique=True),
            callback='parse_response',
            follow=True)
    ]
    
    def parse_response(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        for link in soup.find_all('a'):
            
            if self.isMagnetLink(link):
                
                magnet_link_item = MagnetLink()
                
                magnet_link_item['magnetlink'] = link.get('href')
                magnet_link_item['origin_link'] = response.url
                if self.getMagnetURNType(link.get('href')) is 'bith':
                    magnet_link_item['xturntype'] = self.getMagnetURNType(link.get('href'))
                    
                    
                yield magnet_link_item
                
    def isMagnetLink(self, checklink):
        
        if (   type(checklink.get('href')) is str   )  and (   checklink.get('href').startswith('magnet:')   ) :
            return True
        else:
            return False
        
    def isAcceptedMagnetLink(self,checklink):
        pass
    
    def getMagnetURNType(self, checklink):
        """classify type of magnet link: torrent, ed2k"""
        ACCEPTED_XT = { 'ed2k': 'urn:ed2k',
                    'bith': 'urn:btih',
                    'sha1': 'urn:sha1',
                    'md5' : 'urn:md5',
                    'kzhash': 'urn:kzhash',
                    'tigertree' : 'urn:tree:tiger',
                    'bitprint': 'urn:bitprint'
                    }
    
        urn_type = str()
        urn_types = []
        #find max xt length
        xtTypeMaxLength = 0
        for xttype in ACCEPTED_XT:
            if xtTypeMaxLength < len(xttype):
                xtTypeMaxLength = len(xttype)
           
        xtFirstIndex = checklink.find('xt=urn:')
        
        slice = (checklink[ xtFirstIndex+7   :xtFirstIndex+6+xtTypeMaxLength])
        #print (slice)
        for key,value in ACCEPTED_XT.items():
            if value[4:] in slice:
                return key
                        
