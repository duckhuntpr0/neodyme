# -*- coding: utf-8 -*-

import neodyme

magnetlink = ["magnet:?xt=urn:ed2k:kvdfvdfvil",
              "magnet:?xt=urn:btih:e647384db9db04a871553abd1b6d7ddfaef480c1&dn=A+Place+At+The+Table--Full+Movie&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969",
              "magnet:?xt=urn:btih:13c6bab45b693fbe1fbebbe70a4d70c7f1fb731d&dn=%28Official%29+LeapdroidVMInstallerFull&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969"
              ]

for each in magnetlink:
    data = neodyme.magnetpy.parse_magnet(each) #.parse_magnet(each).data
    print(data.data)
    if 'tr' in data.data.keys():
        trackers_found = data.data['tr']
        print(type(data.data['tr'])) 
        print(data.data['tr'])
        for tracker in trackers_found:
            print(tracker)
#         print(data.data['xt'][4:data.data['xt'].rfind(':')])
#         
#         
#         
#         
#         print (data.data.keys())
#         print( data.data['xt'][data.data['xt'].rfind(':')+1:] )
#     else:
#         print( data.data['xt'][data.data['xt'].rfind(':')+1:] )

# from urllib.parse import unquote_plus as UrlUnquote
# 
# from urllib.parse import urlparse, parse_qs
# 
# class MagnetUrl(object):
#     url = None
#     xturn_delimiter = ':'
# 
#     def __init__(self, url):
#         self.url = url
# 
#     @property
#     def files(self):
#         '''Returns list of files contained in magnet url
#         Files are dicts with following fields:
#         hash_type
#         hash
#         data_size
#         display_name
#         All fields are currently optional and can be None'''
#         files = []
#         index = 0
#         #: lambda checks if entry is valid
#         has_data = lambda e: any([x is not None for x in list(e.values())])
#         entry = self.__file_entry(0)
#         while has_data(entry):
#             files.append(entry)
#             index += 1
#             entry = self.__file_entry(index)
#         return files
# 
#     @property
#     def trackers(self):
#         return self.data.get('tr', [])
# 
#     @property
#     def acceptable_sources(self):
#         return self.data.get('xs', [])
# 
#     # Private side of implementation
#     @property
#     def data(self):
#         '''Tries to parse url, implements simple caching of value
#         to avoid reparsing on every property's access'''
#         if hasattr(self, '_data'):
#             return getattr(self, '_data')
#         self._data = self.__parse(self.url)
#         return self._data
# 
#     def __parse(self, url):
#         parsed = urlparse(url)
#         if parsed.scheme != 'magnet':
#             return {}
#         #: There are differencies in behaviour of urlparse
#         #: between interpreters and python versions so we must assume
#         #: that magnet data may be in these two properties.
#         data = parse_qs(parsed.query or parsed.path[1:])
#         query_data = {}
#         for param_name, values_list in list(data.items()):
#             #we're unpacking values
#             if len(values_list) == 1:
#                 query_data[param_name] = values_list[0]
#             else:
#                 query_data[param_name] = values_list
#         return query_data
# 
#     def __display_name(self, index=None):
#         return self.data_index('dn', index)
# 
#     def data_index(self, fieldname, index=None):
#         data_fieldname = "%s.%s" % (fieldname, index) if index else fieldname
#         return self.data.get(data_fieldname)
# 
#     def __hash_type(self, index=None):
#         ''' Returns type of hash used in magnet link. i.e for 
#         xt=urn:btih:hash_value "btih" is returned.
#         In case of multi-partial names like:
#         xt=urn:tree:tiger:hash_value
#         concatenation is returned, 'tree tiger' in this case.'''
#         xturn = self.data_index('xt', index)
#         if not xturn:
#             return
#         return ' '.join(xturn.split(self.xturn_delimiter)[1:-1])
# 
#     def __hash(self, index=None):
#         '''Returns hash value used in magnet link. Currently we assume that
#         hash is the last part of xt(.<index>) URN.
#         For: 
#         xt=urn:tree:tiger:hash_value
#         function returns hash_value or none
#         '''
#         xturn = self.data_index('xt', index)
#         if not xturn:
#             return
#         return xturn.split(self.xturn_delimiter)[-1]
# 
#     def __data_size(self, index=None):
#         '''Tries to return data-size if is provided'''
#         return self.data_index('xl', index)
# 
#     def __file_entry(self, index):
#         return dict(
#                     display_name=self.__display_name(index),
#                     data_size=self.__data_size(index),
#                     hash_type=self.__hash_type(index),
#                     hash=self.__hash(index)
#                     )
# 
# 
# 
# def getMagnetURNType(checklink):
#     """classify type of magnet link: torrent, ed2k"""
#     ACCEPTED_XT = { 'ed2k': 'urn:ed2k',
#                 'bith': 'urn:btih',
#                 'sha1': 'urn:sha1',
#                 'md5' : 'urn:md5',
#                 'kzhash': 'urn:kzhash',
#                 'tigertree' : 'urn:tree:tiger',
#                 'bitprint': 'urn:bitprint'
#                 }
# 
#     urn_type = str()
#     urn_types = []
#     #find max xt length
#     xtTypeMaxLength = 0
#     for xttype in ACCEPTED_XT:
#         if xtTypeMaxLength < len(xttype):
#             xtTypeMaxLength = len(xttype)
#        
#     xtFirstIndex = checklink.find('xt=urn:')
#     
#     slice = (checklink[ xtFirstIndex+7   :xtFirstIndex+6+xtTypeMaxLength])
#     #print (slice)
#     for key,value in ACCEPTED_XT.items():
#         if value[4:] in slice:
#             return key
#         
# 
#     
# 
# # magnetlink = ["magnet:?xt=urn:btih:e421bf49cab8d22ad88aaa279c4a5e0bda42b21c&dn=%D0%90%D0%BB%D0%B8%D1%81%D0%B0.%D0%9E%D0%BA%D0%BD%D0%B0+%D0%BE%D1%82%D0%BA%D1%80%D0%BE%D0%B9+2006.+%285-%D0%B9+%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB%2C+2006+%D0%B3.%29+%D0%BF%D0%BE%D0%B6%D0%B0%D1%82%D0%BE+%D0%B2+DivX",
# #               "magnet:?xt=urn:ed2k:kil",
# #               "magnet:?xt=urn:btih:c12fe1c06bba254a9dc9f519b335aa7c1367a88a&dn=kukifitta.avi",
# #               "xt.1=urn:sha1:YNCKHTQCWBTRNJIV4WNAE52SJUQCZO5C&xt.2=urn:sha1:TXGCZQTH26NL6OUQAJJPFALHG2LTGBC",
# #               "&xt=urn:md5:D41D8CD98F00B204E9800998ECF8427E",
# #               "magnet:?xt=urn:ed2k:kvdfvdfvil",
# #               "magnet:?xt=urn:btih:e421bf49cab8d22ad88aaa279c4a5e0bda42b21c",
# #               "magnet:?xt=urn:btih:e421bf49cab3d22ad88aaa279c4a5e0bda42b21c&dn=%D0%90%D0%BB%D0%B8%D1%81%D0%B0.%D0%9E%D0%BA%D0%BD%D0%B0+%D0%BE%D1%82%D0%BA%D1%80%D0%BE%D0%B9+2006.+%285-%D0%B9+%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB%2C+2006+%D0%B3.%29+%D0%BF%D0%BE%D0%B6%D0%B0%D1%82%D0%BE+%D0%B2+DivX"
# #               ]
# # count = 0
# # for each in magnetlink:
# #     #print()
# #     #count = 
# #     if (getMagnetURNType(each) is 'bith') or (getMagnetURNType(each) is 'ed2k') or (getMagnetURNType(each) is 'sha1' ) or (getMagnetURNType(each) is 'md5' ) :
# #         count = magnetlink.index(each)
# #         print( str(count) + " " + getMagnetURNType(each) )
#         
# magnetlink = ["magnet:?xt=urn:ed2k:kvdfvdfvil","magnet:?xt=urn:btih:e647384db9db04a871553abd1b6d7ddfaef480c1&dn=A+Place+At+The+Table--Full+Movie&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969","magnet:?xt=urn:btih:13c6bab45b693fbe1fbebbe70a4d70c7f1fb731d&dn=%28Official%29+LeapdroidVMInstallerFull&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969"]
# 
# 
# for each in magnetlink:
#     each = MagnetUrl(each)
#     print(each.data)
# 
# 
# display_name = str()
# 
# if "xt=urn:btih:" in magnetlink:
#     if "&dn=" in magnetlink:            
#         display_name =  magnetlink[magnetlink.find("&dn=")+4:]
#         if '&' in display_name:
#             display_name = display_name[:display_name.find('&')]
#         else:
#             display_name = display_name[:]
#         print( UrlUnquote(display_name)  )
# 
# 
# 
# print()
#     