# -*- coding: utf-8 -*-

import neodyme2ctrl
from time import sleep

jalla = neodyme2ctrl.ScrapyController()

while True:
    print("Pausing")
    jalla.PauseEngine()
    
    sleep(1800)
    
    print("Unpausing")
    jalla.UnPauseEngine()
    
    sleep(600)
