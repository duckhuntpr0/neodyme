# -*- coding: utf-8 -*-

import telnetlib

class ScrapyController:
    def __init__(self,telnetport=6023,telnethost="127.0.0.1"):
        self.tnport = telnetport
        self.tnhost = telnethost
        self.tnconnection = telnetlib.Telnet()
        print("Jalla!")
        
    
    def PauseEngine(self):
        self.tnconnection.open(self.tnhost, self.tnport, 10)
        self.tnconnection.read_until(b">>> ", 5)
        self.tnconnection.write(b"engine.pause()\n")
        self.tnconnection.read_until(b">>> ", 5)
        self.tnconnection.close()
        
        return
    
    def UnPauseEngine(self):
        self.tnconnection.open(self.tnhost, self.tnport, 10)
        self.tnconnection.read_until(b">>> ", 5)
        self.tnconnection.write(b"engine.unpause()\n")
        self.tnconnection.read_until(b">>> ", 5)
        self.tnconnection.close()
        
        return
        
    