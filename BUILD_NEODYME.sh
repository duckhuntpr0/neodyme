#!/bin/bash
# build neodyme

docker build --force-rm --label neodyme --compress --tag neodyme -f neodyme_dockerfile .
