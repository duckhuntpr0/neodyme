#!/bin/sh

mkdir -p /tmp/ramdisk/polipo-cache

# Set the timezone. Base image does not contain the setup-timezone script, so an alternate way is used.
if [ "$SET_CONTAINER_TIMEZONE" = "true" ]; then
    cp /usr/share/zoneinfo/${CONTAINER_TIMEZONE} /etc/localtime && \
	echo "${CONTAINER_TIMEZONE}" >  /etc/timezone && \
	echo "Container timezone set to: $CONTAINER_TIMEZONE"
else
	echo "Container timezone not modified"
fi
 
# Force immediate synchronisation of the time and start the time-synchronization service.
# In order to be able to use ntpd in the container, it must be run with the SYS_TIME capability.
# In addition you may want to add the SYS_NICE capability, in order for ntpd to be able to modify its priority.
ntpd -s

mkdir /tmp/ramdisk && mount -t tmpfs -o size=128M tmpfs /tmp/ramdisk

tmux -2 \
  new-session -s 'NEODYME' -n 'DASHBOARD' "printf '\033]2;IFTOP\033\\' && iftop -NpP ; read" \; \
  set-option -g history-limit 500 \; \
  split-window -h "printf '\033]2;POLIPO\033\\' && sleep 10 && polipo; read" \; \
  split-window "printf '\033]2;PRIVOXY\033\\' && sleep 4 && privoxy --no-daemon /etc/privoxy/config ; read" \; \
  split-window "printf '\033]2;DNSMASQ\033\\' && sleep 2 && dnsmasq -q -d -z -a 127.0.0.1 -i lo --server 127.0.0.1#5353 --cache-size=500 ; read" \; \
  split-window "printf '\033]2;TOR\033\\' && tor ; read" \; \
  select-pane -t 0 \; \
  split-window "printf '\033]2;SHELL\033\\' && cd /root/ && /bin/sh"