#!/bin/bash
# build neodyme

TIMEZONE=$(cat /etc/timezone)
RAM_LIMIT=512m
CPU_LIMIT=0.50
NET_IN_LIMIT=1mb
NET_OUT_LIMIT=1mb

docker run --cap-add=NET_ADMIN -d -t --rm -v $PWD/volume:/root/volume --memory=$RAM_LIMIT --oom-kill-disable --cpus=$CPU_LIMIT --name neodyme -h neodyme --privileged -e TZ=$TIMEZONE -e NET_IN_LIMIT=$NET_IN_LIMIT -e NET_OUT_LIMIT=$NET_OUT_LIMIT --dns=127.0.0.1 neodyme:latest
#docker run -d -t --cidfile container_id --rm --name neodyme --dns=127.0.0.1 neodyme:latest
